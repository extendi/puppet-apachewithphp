class apachewithphp::facegroup {
  apache::vhost { 'www.pulsarplatform.com':
    serveraliases => ['pulsarplatform.com'],
    port => '80',
    docroot => '/mnt/apps/ftpusers/pulsarplatform/cms',
    docroot_owner => 'ftpuser',
    docroot_group => 'ftpgroup',
    rewrites => [
      {
        comment      => 'Redirect no-www to www',
        rewrite_cond => ['%{HTTP_HOST} ^(pulsarplatform.com) [NC]'],
        rewrite_rule => ['^/(.*)$ http://www.pulsarplatform.com/$1 [L,R=301]']
      }
    ],
    override => 'All'
  }

  apache::vhost { 'pulsarblog.extendi.it':
    port => '80',
    docroot => '/mnt/apps/pulsarplatform',
    docroot_owner => 'ubuntu',
    docroot_group => 'ubuntu',
    override => 'All'
  }

  apache::vhost { 'www.facegroup.com':
    serveraliases => ['facegroup.com', 'www.facegroup.co.uk', 'facegroup.co.uk'],
    port => '80',
    docroot => '/mnt/apps/ftpusers/facegroup/site',
    docroot_owner => 'ftpuser',
    docroot_group => 'ftpgroup',
    rewrites => [
      {
        comment      => 'Redirect no-www to www',
        rewrite_cond => ['%{HTTP_HOST} !^www.facegroup.com$ [NC]'],
        rewrite_rule => ['^/(.*)$ http://www.facegroup.com/$1 [L,R=301]']
      }
    ],
    override => 'All'
  }
}